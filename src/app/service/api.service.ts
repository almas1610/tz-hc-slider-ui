import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { ISlider } from "../shared/modules/slider/types/slider.interface";

const sliders = [
  { id: 1, image: '/assets/img1.jpg', url: 'http://localhost:4200/assets/img1.jpg', priority: 1.4 },
  { id: 2, image: '/assets/img2.jpg', url: 'http://localhost:4200/assets/img2.jpg', priority: 1.7 },
  { id: 3, image: '/assets/img5.jpg', url: 'http://localhost:4200/assets/img5.jpg', priority: 2.4 },
];

@Injectable({providedIn: "root"})
export class ApiService {
  getSliders(): Observable<ISlider[]> {
    return of(sliders)
  }
}
