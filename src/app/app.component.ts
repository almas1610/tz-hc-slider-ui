import { Component, OnInit } from '@angular/core';
import { ApiService } from './service/api.service';
import { Observable } from 'rxjs';
import { ISlider } from './shared/modules/slider/types/slider.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  sliders!: ISlider[]

  constructor(private apiServ: ApiService) {}

  ngOnInit(): void {
    this.apiServ.getSliders().subscribe(s => this.sliders = s)
  }
}
