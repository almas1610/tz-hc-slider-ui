import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PersistenceService {
  #data: BehaviorSubject<{ key: string; data: any }> = new BehaviorSubject<{
    key: string;
    data: any;
  }>({ key: '', data: '' });

  set(key: string, data: any): void {
    this.#data.next({key, data});
    try {
      sessionStorage.setItem(key, JSON.stringify(this.#data.value.data));
    } catch (err) {
      console.log('Error saving to localStorage', err);
    }
  }

  get(): any {
    try {
      let value = sessionStorage.getItem(this.#data.value.key)
      return parseInt(value!) // <-- fix "!"
    } catch (err) {
      console.log('Error getting data from localStorage', err);
      return null;
    }
  }

  delete(key: string): void {
    sessionStorage.removeItem(key);
  }
}
