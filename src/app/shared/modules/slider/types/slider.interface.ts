export interface ISlider {
  id: number;
  image: string;
  url: string;
  priority: number;
}
