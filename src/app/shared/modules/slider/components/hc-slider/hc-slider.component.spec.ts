import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HcSliderComponent } from './hc-slider.component';

describe('HcSliderComponent', () => {
  let component: HcSliderComponent;
  let fixture: ComponentFixture<HcSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HcSliderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HcSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
