import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ISlider } from '../../types/slider.interface';
import { PersistenceService } from 'src/app/shared/services/persistence.service';

@Component({
  selector: 'hc-slider',
  templateUrl: './hc-slider.component.html',
  styleUrls: ['./hc-slider.component.scss'],
})
export class HcSliderComponent implements OnInit, OnDestroy {
  @Input('sliders') propsSliders: ISlider[] = []; // default empty []
  @Input('delay') propsDelay: number = 2000; // default 2 sec
  @Input('autoPlay') propsAutoPlay: boolean = false; // default turned off
  @Input('withPriority') propsWithPriority: boolean = false; // default do not check priority of slides

  currentIndex: number = 0;
  slidersWithPriority!: ISlider[];
  timeId: number = 0;

  constructor(private persistanceService: PersistenceService) {}

  ngOnInit(): void {
    if (this.propsAutoPlay) {
      this.autoPlaySlider();
    }
    if (this.propsWithPriority) {
      this.slidersWithPriority = this.getPrioritySlide();
    }
  }

  getPrioritySlide(): ISlider[] {
    let max!: ISlider;
    let res = [];
    for (let i = 0; i < this.propsSliders.length; i++) {
      if (this.propsSliders[i].priority >= 2.0) {
        max = this.propsSliders[i];
      }
    }
    for (let i = 0; i < this.propsSliders.length; i++) {
      max.priority > this.propsSliders[i].priority
        ? res.push(...[max, this.propsSliders[i]])
        : null;
    }
    return res;
  }

  autoPlaySlider(): void {
    this.timeId = window.setInterval(() => {
      this.nextSlide();
    }, this.propsDelay);
  }

  getCurrentSlideUrl(): string {
    if (!this.propsSliders.length) {
      return '/assets/img404.jpg';
    }
    return this.propsWithPriority
      ? this.slidersWithPriority[this.currentIndex].image
      : this.propsSliders[this.currentIndex].image;
  }

  // Проще можно обернуть в тэг <a> и возвращать url в href, _blank
  getUrlAddress(): void {
    if (this.propsWithPriority) {
      window.open(this.slidersWithPriority[this.currentIndex].url);
      return
    }
    window.open(this.propsSliders[this.currentIndex].url);
  }


  nextSlide(): void {
    const isLastSlide =
      this.currentIndex ===
      (this.propsWithPriority
        ? this.slidersWithPriority.length - 1
        : this.propsSliders.length - 1);
    const idx = isLastSlide ? 0 : this.currentIndex + 1;

    this.currentIndex = idx
  }

  previousSlide(): void {
    const isZeroSlide = this.currentIndex === 0;
    const idx = isZeroSlide
      ? this.propsWithPriority
        ? this.slidersWithPriority.length - 1
        : this.propsSliders.length - 1
      : this.currentIndex - 1;

    this.currentIndex = idx;
  }

  goToSlide(i: number): void {
    this.currentIndex = i;
  }

  ngOnDestroy(): void {
    if (this.timeId) {
      window.clearInterval(this.timeId);
    }
  }
}
