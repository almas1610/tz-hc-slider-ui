import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HcSliderComponent } from './components/hc-slider/hc-slider.component';

@NgModule({
  imports: [CommonModule],
  exports: [HcSliderComponent],
  declarations: [HcSliderComponent],
})
export class SliderModule {}
